from users.models import User
from rest_framework import serializers

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id',
                  'name',
                  'email',
                  'pengu_email',
                  'fcm_token',
                  'version',
                  'platform'
                    )