from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
import uuid

class CustomUserAccountAdapter(DefaultAccountAdapter):
    def save_user(self, request, user, form, commit=True):
            """
            Saves a new `User` instance using information provided in the
            signup form.
            """
            from  allauth.account.utils import user_email, user_field, user_username

            PenguUUID = uuid.uuid4()
            data = request.data
            email = data.get("email")
            username = data.get("username")
            name = data.get("name")
            pengu_email = email.split('@')[0] + '-' + str(PenguUUID)  + '@mail.pengu-app.com'
            user_email(user, email)
            user_username(user, username)
            if name:
                user_field(user, "name", name)
            if pengu_email:
                user_field(user, "pengu_email", pengu_email)
            if "password1" in data:
                user.set_password(data["password1"])
            else:
                user.set_unusable_password()
            self.populate_username(request, user)
            if commit:
                # Ability not to commit makes it easier to derive from
                # this adapter by adding
                user.save()
            return user

class CustomSocialUserAccountAdapter(DefaultSocialAccountAdapter):
    def populate_user(self, request, sociallogin, data):

        from  allauth.account.utils import user_email, user_field, user_username
        from  allauth.account.utils import valid_email_or_none

        PenguUUID = uuid.uuid4()
        username = data.get("username")
        first_name = data.get("first_name")
        last_name = data.get("last_name")
        email = data.get("email")
        name = f'{first_name} {last_name}'
        user = sociallogin.user
        pengu_email = email.split('@')[0] + '-' + str(PenguUUID)  + '@mail.pengu-app.com'
        user_username(user, username or "")
        user_email(user, valid_email_or_none(email) or "")
        name_parts = (name or "").partition(" ")
        if name:
            user_field(user, "name", name)
        if pengu_email:
            user_field(user, "pengu_email", pengu_email)
        return user
