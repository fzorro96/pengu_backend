from datetime import datetime
from financial_tool.models import Budget, Movement, Notification
from django.db.models import Sum

from users.models import User
from utils.notifications import Notifications


def sum_movements(notification_frequency, user_id, categories):
    if notification_frequency == Budget.NotificationFrequency.MONTHLY.value:
        final_sum = Movement.objects.filter(user=user_id, category__in=categories,
                                            created_at__year=datetime.now().year,
                                            created_at__month=datetime.now().month). \
            aggregate(Sum('amount')).get("amount__sum", 0)
    elif notification_frequency == Budget.NotificationFrequency.DAILY.value:
        final_sum = Movement.objects.filter(user=user_id, category__in=categories,
                                            created_at__year=datetime.now().year,
                                            created_at__day=datetime.now().day,
                                            created_at__month=datetime.now().month). \
            aggregate(Sum('amount')).get("amount__sum", 0)
    elif notification_frequency == Budget.NotificationFrequency.YEARLY.value:
        final_sum = Movement.objects.filter(user=user_id, category__in=categories,
                                            created_at__year=datetime.now().year). \
            aggregate(Sum('amount')).get("amount__sum", 0)
    return final_sum


def check_notification(notification_frequency, user_id, budget_id):
    if notification_frequency == Budget.NotificationFrequency.MONTHLY.value:
        notification = Notification.objects.filter(user=user_id,
                                                   budget=budget_id,
                                                   created_at__year=datetime.now().year,
                                                   created_at__month=datetime.now().month)
    elif notification_frequency == Budget.NotificationFrequency.DAILY.value:
        notification = Movement.objects.filter(user=user_id,
                                               budget=budget_id,
                                               created_at__year=datetime.now().year,
                                               created_at__day=datetime.now().day,
                                               created_at__month=datetime.now().month)
    elif notification_frequency == Budget.NotificationFrequency.YEARLY.value:
        notification = Movement.objects.filter(user=user_id,
                                               budget=budget_id,
                                               created_at__year=datetime.now().year)
    if notification:
        return True
    return False


def check_budget_notification(request, **kwargs):
    print("revisando budget")
    user_id = kwargs.get("user_id", False)
    category = request.data.get("category", False)
    if user_id and category:
        budgets = Budget.objects.filter(user=user_id, categories__in=[category]).all().values()
        for budget in budgets:
            sum_total_movements = sum_movements(budget["notification_frequency"], user_id,
                                                [i["id"] for i in
                                                 (Budget.objects.get(id=budget["id"]).categories.values())]
                                                )
            if sum_total_movements > budget["limit"] and not check_notification(budget["notification_frequency"],
                                                                                user_id, budget["id"]):
                user = User.objects.filter(id=user_id).values()[0]
                if user.get("fcm_token", False):
                    title = f"Alcanzaste tu límite del presupuesto {budget['name']}"
                    body = f"""{user.get('name')} tu presupuesto {budget['name']} está completo para el periodo de tiempo que configuraste."""
                    Notifications(**{"user_id": user_id, "body": body, "title": title, "budget_id": budget["id"],
                                     "device_token": user.get("fcm_token"), "notification_type": "BUDGET_LIMIT",
                                     "notification_channel": "PUSH"}).send_notification()
