from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
import json
from financial_tool.serializers import MovementSerializer, CategorySerializer, FinancialEntitySerializer, \
    ExtractorTypeSerializer, ExtractorSerializer, MovementWorkflowSerializer, CardSerializer, BudgetSerializer
from financial_tool.models import Movement, Category, FinancialEntity, ExtractorType, Extractor, \
    MovementWorkflow, Card, Budget

from users.models import User
from users.serializers import UserSerializer
from utils.notifications import Notifications
from financial_tool.utils.functions import check_budget_notification


class MovementViewSet(viewsets.ModelViewSet):
    filterset_fields = {
        'amount': ['gte', 'lte'],
        'place': ['icontains'],
        'card': ['exact'],
        'movement_date': ["gte", "lte"],
        'movement_type': ['exact'],
        'payment_type': ['exact'],
        'is_manual': ['exact'],
        'category': ['exact'],
        'financial_entity': ['exact']
    }
    queryset = Movement.objects.all().order_by('id')
    serializer_class = MovementSerializer

    def get_queryset(self):
        return super().get_queryset().filter(user=self.kwargs.get('user_id')).all()

    def update(self, request, *args, **kwargs):
        instance = self.queryset.get(pk=kwargs.get('pk'))
        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        check_budget_notification(request, **kwargs)
        return Response(serializer.data)

    def partial_update(self, request, *args, **kwargs):
        instance = self.queryset.get(pk=kwargs.get('pk'))
        serializer = self.serializer_class(instance, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        check_budget_notification(request, **kwargs)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        response = super().create(request)
        check_budget_notification(request, **kwargs)
        return response


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all().order_by('id')
    serializer_class = CategorySerializer

    def get_queryset(self):
        return super().get_queryset().filter(user=self.kwargs.get('user_id')).all()


class BudgetViewSet(viewsets.ModelViewSet):
    queryset = Budget.objects.all().order_by('id')
    serializer_class = BudgetSerializer

    def get_queryset(self):
        return super().get_queryset().filter(user=self.kwargs.get('user_id')).all()


class FinancialEntityViewSet(viewsets.ModelViewSet):
    queryset = FinancialEntity.objects.all().order_by('id')
    serializer_class = FinancialEntitySerializer


class CardViewSet(viewsets.ModelViewSet):
    filter_fields = (
        'card_number',
        'user'
    )
    queryset = Card.objects.all().order_by('id')
    serializer_class = CardSerializer


class ExtractorTypeViewSet(viewsets.ModelViewSet):
    queryset = ExtractorType.objects.all().order_by('id')
    serializer_class = ExtractorTypeSerializer


class ExtractorViewSet(viewsets.ModelViewSet):
    queryset = Extractor.objects.all().order_by('id')
    serializer_class = ExtractorSerializer


class MovementWorkflowViewSet(viewsets.ModelViewSet):
    queryset = MovementWorkflow.objects.all().order_by('id')
    serializer_class = MovementWorkflowSerializer


class UserViewSet(viewsets.ModelViewSet):
    filter_fields = (
        'pengu_email',
    )
    queryset = User.objects.all().order_by('id')
    serializer_class = UserSerializer

    @action(detail=False, methods=['post'], url_path='return_user')
    def return_user(self, request):
        user = User.objects.get(id=request.auth.payload.get('user_id'))
        serializer = self.get_serializer(user)
        return Response(serializer.data)

    @action(detail=False, methods=['post'], url_path='send_notification')
    def send_notification(self, request):
        notification = json.loads(request.body.decode(encoding='UTF-8'))
        response = Notifications(**notification).send_notification()
        return Response(response.json())
