from financial_tool.models import Movement, Category, FinancialEntity, ExtractorType, Extractor, \
    MovementWorkflow, Card, Budget
from rest_framework import serializers


class CardSerializer(serializers.ModelSerializer):
    class Meta:
        model = Card
        fields = '__all__'


class MovementSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movement
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'


class BudgetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Budget
        fields = '__all__'


class FinancialEntitySerializer(serializers.ModelSerializer):
    class Meta:
        model = FinancialEntity
        fields = '__all__'


class ExtractorTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExtractorType
        fields = '__all__'


class ExtractorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Extractor
        fields = '__all__'


class MovementWorkflowSerializer(serializers.ModelSerializer):
    class Meta:
        model = MovementWorkflow
        fields = '__all__'
