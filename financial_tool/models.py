from django.db import models
from users.models import User

from django.utils.translation import gettext_lazy as _


class FinancialEntity(models.Model):
    name = models.CharField(max_length=50)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'financial_entity'
        verbose_name_plural = "financial entities"

    def __str__(self):
        return self.name


class ExtractorType(models.Model):
    extractor_type = models.CharField(max_length=50)
    configuration = models.JSONField()
    active = models.BooleanField(default=True)
    financial_entity = models.ForeignKey(FinancialEntity,
                                         on_delete=models.CASCADE,
                                         related_name="extractor_types",
                                         null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'extractor_type'
        verbose_name_plural = "extractor types"

    def __str__(self):
        return self.extractor_type


class Extractor(models.Model):
    configuration = models.JSONField()
    extractor_type = models.ForeignKey(ExtractorType,
                                       on_delete=models.CASCADE,
                                       related_name="extractors",
                                       null=True)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="extractors",
                             null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'extractor'
        verbose_name_plural = "extractors"

    def __str__(self):
        return f'{self.user.name}, {self.extractor_type.extractor_type}'


class Category(models.Model):
    name = models.CharField(max_length=50)
    image_url = models.CharField(max_length=255, null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'category'
        verbose_name_plural = "categories"

    def __str__(self):
        return self.name


class Card(models.Model):
    CARD_TYPES = [
        ('1', 'DEBIT'),
        ('2', 'CREDIT'),
    ]

    card_number = models.CharField(max_length=50)
    card_type = models.CharField(max_length=1, choices=CARD_TYPES, null=True)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="cards")
    financial_entity = models.ForeignKey(FinancialEntity,
                                         on_delete=models.CASCADE,
                                         related_name="cards")
    is_manual = models.BooleanField(default=False)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'card'
        verbose_name_plural = "cards"

    def __str__(self):
        return self.card_number


class Movement(models.Model):
    MOVEMENT_TYPE = (
        ('purchase', 'purchase'),
        ('withdrawal', 'withdrawal'),
        ('payment', 'payment'),
        ('other', 'other'),
        ('undefined', 'undefined')
    )
    PAYMENT_TYPE = (
        ('cash', 'cash'),
        ('card', 'card'),
        ('pse', 'pse'),
        ('other', 'other'),
        ('undefined', 'undefined')
    )
    place = models.TextField(max_length=255)
    amount = models.FloatField()
    movement_date = models.DateTimeField()
    card = models.ForeignKey(Card,
                             on_delete=models.CASCADE,
                             related_name="cards",
                             null=True)
    description = models.TextField(max_length=255)
    movement_type = models.CharField(max_length=50, choices=MOVEMENT_TYPE, default='undefined')
    payment_type = models.CharField(max_length=50, choices=PAYMENT_TYPE, default='undefined')
    is_hidden = models.BooleanField(default=False)
    is_manual = models.BooleanField(default=False)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="movements")
    extractor = models.ForeignKey(Extractor,
                                  on_delete=models.CASCADE,
                                  related_name="movements",
                                  null=True)
    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 related_name="%(class)s_requests_created",
                                 null=True)
    financial_entity = models.ForeignKey(FinancialEntity,
                                         on_delete=models.CASCADE,
                                         related_name="movements",
                                         null=True)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'movement'
        verbose_name_plural = "movements"

    def __str__(self):
        return f'{self.place}, {self.amount}'


class CategoryRule(models.Model):
    class RulesTypes(models.TextChoices):
        EQUAL = 1, _('equal')
        CONTAINS = 2, _('contains')

    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="categories_rules")
    category = models.ForeignKey(Category,
                                 on_delete=models.CASCADE,
                                 related_name="movements")
    rule_type = models.IntegerField(choices=RulesTypes.choices, default=RulesTypes.EQUAL)
    rule = models.CharField(max_length=255)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'category_rule'
        verbose_name_plural = "category_rules"


class Budget(models.Model):
    class NotificationFrequency(models.IntegerChoices):
        OFF = 1, _('OFF')
        DAILY = 2, _('DAILY')
        MONTHLY = 3, _('MONTHLY')
        YEARLY = 4, _('YEARLY')

    limit = models.FloatField()
    categories = models.ManyToManyField(Category)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name="budgets")
    notification_frequency = models.IntegerField(choices=NotificationFrequency.choices,
                                                 default=NotificationFrequency.MONTHLY)
    active = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    name = models.CharField(max_length=50)

    class Meta:
        managed = True
        db_table = 'budget'
        verbose_name_plural = "budgets"

    def __str__(self):
        return self.name


class Notification(models.Model):
    class NotificationType(models.IntegerChoices):
        GOOGLE_CODE = 1, _('GOOGLE_CODE')
        BUDGET_LIMIT = 2, _('BUDGET_LIMIT')

    class NotificationChannel(models.IntegerChoices):
        PUSH = 1, _('PUSH')
        EMAIL = 2, _('EMAIL')
        WHATSAPP = 3, _('WHATSAPP')

    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name='notifications',
                             null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    notification_type = models.IntegerField(choices=NotificationType.choices,
                                            default=NotificationType.BUDGET_LIMIT)
    notification_channel = models.IntegerField(choices=NotificationChannel.choices,
                                               default=NotificationChannel.PUSH)
    body = models.CharField(max_length=500)
    response = models.CharField(max_length=500)
    title = models.CharField(max_length=50)
    budget = models.ForeignKey(Budget,
                               on_delete=models.CASCADE,
                               related_name='budgets',
                               null=True)



class MovementWorkflow(models.Model):
    is_received = models.BooleanField(default=False)
    received_date = models.DateTimeField()
    message = models.CharField(max_length=255, null=True)
    is_user = models.BooleanField(default=False)
    is_valid_subject = models.BooleanField(default=False)
    is_parsed = models.BooleanField(default=False)
    is_written_in_db = models.BooleanField(default=False)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name='movements_workflows',
                             null=True)
    extractor_type = models.ForeignKey(ExtractorType,
                                       on_delete=models.CASCADE,
                                       related_name='movements_workflows',
                                       null=True)
    movement = models.ForeignKey(Movement,
                                 on_delete=models.CASCADE,
                                 related_name='movements_workflows',
                                 null=True)
    active = models.BooleanField(default=True)
    data = models.TextField(max_length=255, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        managed = True
        db_table = 'movement_workflow'
        verbose_name_plural = "movements workflow"
