from django.urls import path, include
from rest_framework.routers import DefaultRouter
from financial_tool import views

# Create a router and register our viewsets with it.
router = DefaultRouter()
router.register(r'(?P<user_id>\d+)/movements', views.MovementViewSet, basename='movements')
router.register(r'(?P<user_id>\d+)/budgets', views.BudgetViewSet, basename='budgets')
router.register(r'(?P<user_id>\d+)/categories', views.CategoryViewSet, basename='categories')
router.register(r'financial-entity', views.FinancialEntityViewSet, basename='financial-entities')
router.register(r'extractor-type', views.ExtractorTypeViewSet, basename='extractors-type')
router.register(r'extractor', views.ExtractorViewSet, basename='extractors')
router.register(r'workflow', views.MovementWorkflowViewSet, basename='workflows')
router.register(r'user', views.UserViewSet, basename='user'),
router.register(r'card', views.CardViewSet, basename='card')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]