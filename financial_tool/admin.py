from django.contrib import admin
from financial_tool.models import *

admin.site.register(ExtractorType)
admin.site.register(Extractor)
admin.site.register(FinancialEntity)
admin.site.register(Movement)
admin.site.register(Category)
admin.site.register(MovementWorkflow)
admin.site.register(Card)
admin.site.register(Budget)
admin.site.register(Notification)