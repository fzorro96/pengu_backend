from django.apps import AppConfig


class FinancialToolConfig(AppConfig):
    name = 'financial_tool'
