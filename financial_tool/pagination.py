from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from .models import Movement
from collections import Counter

DEFAULT_PAGE = 1
DEFAULT_PAGE_SIZE = 100


class CustomPagination(PageNumberPagination):
    page = DEFAULT_PAGE
    page_size = DEFAULT_PAGE_SIZE
    page_size_query_param = 'page_size'

    def generate_new_movement_keys(self, dict_response):
        tx_amount_list = [x.amount for x in self.page.paginator.object_list]
        tx_date_list = [x.movement_date for x in self.page.paginator.object_list]
        if tx_amount_list and tx_date_list:
            total_tx = sum(tx_amount_list)
            max_amount = max(tx_amount_list)
            min_amount = min(tx_amount_list)
            max_date = max(tx_date_list)
            min_date = min(tx_date_list)
            dict_response.update({"total_tx": total_tx})
            dict_response.update({"max_amount": max_amount})
            dict_response.update({"min_amount": min_amount})
            dict_response.update({"max_date": max_date})
            dict_response.update({"min_date": min_date})
            dict_response.update(
                {"payment_type_count": Counter([x.payment_type for x in self.page.paginator.object_list if
                                                x.payment_type in [c[0] for c in
                                                                   Movement.payment_type.field.choices]])})
            dict_response.update(
                {"movement_type_count": Counter([x.movement_type for x in self.page.paginator.object_list if
                                                 x.movement_type in [c[0] for c in
                                                                     Movement.movement_type.field.choices]])})
        return dict_response

    def get_paginated_response(self, data):
        dict_response = {
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'total': self.page.paginator.count,
            'page': int(self.request.GET.get('page', DEFAULT_PAGE)),
            'page_size': int(self.request.GET.get('page_size', self.page_size)),
            'results': data
        }
        if "movements" in str(self.request):
            dict_response = self.generate_new_movement_keys(dict_response)
        return Response(dict_response)
