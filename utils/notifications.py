import requests
import json
from dotenv import load_dotenv
import os
from financial_tool.models import Notification, User, Budget

load_dotenv()


class Notifications:
    def __init__(self, **kwargs):
        self.server_token = kwargs.get("server_token", os.getenv("GOOGLE_SERVER_TOKEN"))
        self.device_token = kwargs.get("device_token")
        self.priority = kwargs.get("priority", "high")
        self.budget_id = kwargs.get("budget_id", None)
        self.title = kwargs.get("title")
        self.body = kwargs.get("body")
        self.user_id = kwargs.get("user_id")
        self.notification_type = kwargs.get("notification_type")
        self.notification_channel = kwargs.get("notification_channel")

    def send_notification(self):
        if self.notification_channel == "PUSH":
            self.__send_push_notification()

    def __send_push_notification(self):
        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'key=' + self.server_token,
        }
        body = {
            'notification': {'title': self.title,
                             'body': self.body
                             },
            'to': self.device_token,
            'priority': self.priority}
        response = requests.post("https://fcm.googleapis.com/fcm/send", headers=headers, data=json.dumps(body))
        Notification.objects.create(user=User.objects.get(id=self.user_id),
                                    budget=Budget.objects.get(id=self.budget_id),
                                    notification_type=Notification.NotificationType[self.notification_type].value,
                                    notification_channel=Notification.NotificationChannel[
                                        self.notification_channel].value,
                                    body=self.body,
                                    title=self.title,
                                    response=response.text)
        return response
