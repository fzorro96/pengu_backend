# Pengu Backend

Pengu Backend financial tool.

## Setup

The first thing to do is to clone the repository:

```bash
$ git clone git@gitlab.com:fzorro96/backend_pengu.git
$ cd backend_pengu
```
Create a virtual environment to install dependencies in and activate it:
```bash
$ virtualenv venv
$ source venv/bin/activate
```

Then install the dependencies:

```bash
(venv) $ pip install -r requitements.txt
```

Create .env file :

```bash
DB_NAME=
DB_USERNAME=
DB_PASSWORD=
DB_HOST=
DB_PORT=
```

Run server:
```bash
(venv)$ python manage.py runserver
```
